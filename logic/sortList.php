<?php

include_once('divideTasksPerPages.php');
session_name("checklist");
session_start();

function sortList() {

    if ($_POST["column"] == "email" && $_POST["sort"] == "up") {

        $arrayOfId = $_SESSION["emailarray"];
        arsort($arrayOfId);

    } else if ($_POST["column"] == "email" && $_POST["sort"] == "down") {

        $arrayOfId = $_SESSION["emailarray"];
        asort($arrayOfId);

    } else if ($_POST["column"] == "username" && $_POST["sort"] == "up") {
        
        $arrayOfId = $_SESSION["namearray"];
        arsort($arrayOfId);
    
    } else if ($_POST["column"] == "username" && $_POST["sort"] == "down") {
        
        $arrayOfId = $_SESSION["namearray"];
        asort($arrayOfId);
    
    } else if ($_POST["column"] == "status" && $_POST["sort"] == "up") {
        $arrayOfId = $_SESSION["statusarray"];
        arsort($arrayOfId);
    } else if ($_POST["column"] == "status" && $_POST["sort"] == "down") {
        $arrayOfId = $_SESSION["statusarray"];
        asort($arrayOfId);
    }
    $tmparray = $_SESSION["tasks"];
    $_SESSION["tasks"] = array();
    foreach ($arrayOfId as $key => $value) {
        $tmpobj = null;
        foreach ($tmparray as $obj) {
            if ($obj["id"] == $key) {
                $tmpobj = $obj;
                break;
            }
        }
        array_push($_SESSION["tasks"], $tmpobj);
    }
    divideForPages();
return $_POST["page"] . "||" . json_encode($_SESSION["tasks"], true) . "||" . json_encode($_SESSION["tasksperpage"], true);
}

echo sortList();