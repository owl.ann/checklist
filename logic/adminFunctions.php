<?php

include('connectToDB.php');
include('divideTasksPerPages.php');

session_name("checklist");
session_start();

function adminFunctions() {

    createConnection();
    
    $id = $_POST["id"];
    
    if ($_POST["change"] == "status") {
        
        foreach ($_SESSION["tasks"] as $key => $task) {
            if ($task["id"] == $id) {
                $_SESSION["tasks"][$key]["done"] = 1;
            }
        }
        $_SESSION["statusarray"][$id] = 1;
    
        $sql = "UPDATE tasks SET done='1' WHERE id=$id";
        if (!makeQuery($sql))
            $result = 'Error: ' . $sql . "\n" . getError() . "\n";
    
    } else if ($_POST["change"] == "delete") {
        
        foreach ($_SESSION["tasks"] as $key => $task) {
            if ($task["id"] == $id) {
                unset($_SESSION["tasks"][$key]);
                break;
            }
        }
        unset($_SESSION["emailarray"]["$id"]);
        unset($_SESSION["statusarray"]["$id"]);
        unset($_SESSION["namearray"]["$id"]);
        
        $sql = "DELETE FROM tasks WHERE id=$id";
        if (!makeQuery($sql))
            $result = "Error: " . $sql . "\n" . getError() . "\n";
        
        divideForPages();
        $result = json_encode($_SESSION["tasksperpage"], true);
    } else if ($_POST["change"] == "update" && $_SESSION["logged_admin"] == 'true') {
        
        foreach ($_SESSION["tasks"] as $key => $task) {
            if ($task["id"] == $id) {
                $_SESSION["tasks"][$key]["task"] = htmlspecialchars($_POST["text"]);
                $_SESSION["tasks"][$key]["edited"] = 1;
            }
        }
        
        $sql = "UPDATE tasks SET task='" . addslashes(htmlspecialchars($_POST["text"])) . "', edited='1' WHERE id=$id";
        
        if (!makeQuery($sql))
            $result = "Error: " . $sql . "\n" . getError() . "\n";
            
    } else {
        $result = 0;
    }
    
    closeConnection();
    
    return $result;
    
}

echo adminFunctions();		