<?php
session_name("checklist");
session_start();

function divideForPages() {

     $arrayOfTasksPerPage = array();
     $numberOfTasks = count($_SESSION["tasks"]);
     $pages = $numberOfTasks % 3 == 0 ? $numberOfTasks / 3.0 : (int)
     ($numberOfTasks / 3) + 1;
     $i = 1;
     $page = 1;
     $arrayOfTasksPerPage[0] = array();
     $arrayOfTasksPerPage[$page] = array();
 
     foreach($_SESSION["tasks"] as $task) {
         if ($i > 3) {
             $page++;
             $arrayOfTasksPerPage[$page] = array();
             $i = 1;
         }
         array_push($arrayOfTasksPerPage[$page], $task["id"]);
         $i++;
     }
     $_SESSION["tasksperpage"] = $arrayOfTasksPerPage;
     $_SESSION["pages"] = $pages;

}