<?php

include ('connectToDB.php');
session_name("checklist");
session_start();

if (isset($_POST['username']) && isset($_POST['email']) &&
	isset($_POST['task'])) {

	createConnection();

	$username = addslashes(htmlspecialchars($_POST['username']));
	$email = addslashes(htmlspecialchars($_POST['email']));
	$task = addslashes(htmlspecialchars($_POST['task']));
	
	$sql = 'insert into tasks (name, email, task, done, edited) values ("' . $username . '",
	"' . $email . '", "' . $task . '", 0, 0)';

	if (makeQuery($sql) !== true) {
		$error = getError();
		closeConnection();
		header('Location: ../view/createtask.php?pagename=CreateTask&task=failed&detail=' . $error);
	}
    $sql = 'select max(id) as id from tasks';
    if (!($result = makeQuery($sql))) {
        $error = getError();
    	closeConnection();
		header('Location: ../view/createtask.php?pagename=CreateTask&task=failed&detail=' . $error);
    } else if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {            
            $obj["id"] = $row["id"];
            $obj["name"] = $username;
            $obj["email"] = $email;
            $obj["task"] = $task;
            $obj["done"] = 0;
            $obj["edited"] = 0;
            $_SESSION["statusarray"][$row["id"]]= 0;
            $emailobj[$row["id"]] = $email;
            $_SESSION["emailarray"][$row["id"]] = $email;
            $_SESSION["namearray"][$row["id"]] = $name;
            array_push($_SESSION["tasks"], $obj);
        }
    }
	closeConnection();
	header('Location: ../view/alltasks.php?pagename=All Tasks&task=created');
}
		