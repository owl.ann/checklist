<?php
include ('logic/connectToDB.php');
session_name("checklist");
session_start();

createConnection();

$sql = "select * from tasks";
$tasks = array();
$namesorting = array();
$emailsorting = array();
$donesorting = array();
if (($result = makeQuery($sql))) {
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$task["id"] = $row["id"];
			$task["name"] = $row["name"];
			$task["email"] = $row["email"];
			$task["task"] = $row["task"];
			$task["done"] = (int)$row["done"];
            $task["edited"] = (int)$row["edited"];
            $statussorting[$row['id']] = $row["done"];
            $namesorting[$row['id']] = $row["name"];
            $emailsorting[$row['id']] = $row["email"];
			array_push($tasks, $task);
		}
	}
}
$_SESSION["statusarray"] = $statussorting;
$_SESSION["namearray"] = $namesorting;
$_SESSION["emailarray"] = $emailsorting;
$_SESSION["tasks"] = $tasks;
header('Location: view/alltasks.php?pagename=All tasks');
?>

