<?php
include('header.php');

?>
<div class="container" style="width: 30%;">
	<form id="form" method="post" action="../logic/login.php">
		<fieldset class="border rounded p-2 m-3">
			<div class="form-group">
				<label for="InputLogin">Username</label>
				<input type="text" name="username" class="form-control"
				id="username"required/>
				<div class="invalid-feedback">Please enter valid username</div>
			</div>
			<div class="form-group">
				<label for="InputPassword">Password</label>
				<input type="password" name="password" class="form-control"
				id="password" required/>
				<div class="invalid-feedback">Please enter valid password</div>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</fieldset>
	</form>
</div>

<script>
let values = '<?php echo $_GET["values"]; ?>';
let password = '<?php echo $_GET["password"]; ?>';
if (values == 'wrong') {
	let elements = document.getElementsByTagName("input");
	for (let obj of elements)
		obj.classList.add("is-invalid");
} else if (password == 'wrong') {
	let element = document.getElementById("password");
	element.classList.add("is-invalid");
}
</script>
<?php include('footer.html') ?>
