<?php
include('header.php');
include('../logic/divideTasksPerPages.php');
include('editTask.html');
?>

<div class="container-fluid overflow-auto min-vw-540">
    <ul class="list-group list-group-flush border rounded m-3 mt-5" style="min-width: 540px;" id="taskslist">
        <li class="list-group-item active">
            <div class="row">
                <div class="col-1">
                </div>
                <div id="statuscolumn" sort="up" class="col-1">Status</div>
                <div id="usernamecolumn" sort="up" class="col-2">Username</div>
                <div id="emailcolumn" sort="up" class="col-2" >Email</div>
                <div class="col-6">Task</div>
            </div>
        </li>
    </ul>
    <nav class="float-right">
        <ul class="pagination">
            <li class="page-item" id="prev">
                <p class="page-link" action="" aria-label="First">
                    <span aria-hidden="true">&laquo;</span>
                </p>
            </li>
            <li class="page-item" id="last">
                <p class="page-link" action="" aria-label="Last">
                    <span aria-hidden="true">&raquo;</span>
                </p>
            </li>
        </ul>
    </nav>
</div>
<?php

divideForPages();
if ($_GET["task"] == "created") {
    $alertMessage = true;
}

?>
<script src="js/createListOfTasks.js">
</script>
<script src="js/sortList.js">
</script>
<script src="js/adminFunctions.js">
</script>
<script>
$(document).ready(function() {
    let successMessage = '<?php echo $alertMessage; ?>';
    if (successMessage == 1)
        alert("Task successfully created!");
    let admin = ('<?php echo $_SESSION["logged_admin"]; ?>' == 'true') ? true : false;
    let arrayOfTasks = <?php echo json_encode($_SESSION["tasks"], true); ?>;
    createList(arrayOfTasks, admin);
    
    let pages = <?php echo $_SESSION["pages"]; ?>;
    let currentElement = document.getElementById("prev");
    for (let i = 1; i <= pages; ++i) {
        let newElement = document.createElement("li");
        newElement.setAttribute("class", "page-item");
        newElement.setAttribute("id", i + "page");
        
        let num = document.createElement("a");
        num.setAttribute("class", "page-link");
        num.setAttribute("href", "#");
        num.innerHTML = i;
        
        newElement.insertAdjacentElement("beforeend", num);
        currentElement.insertAdjacentElement("afterend", newElement);
        currentElement = newElement;
    }
    
    let currentPage = 1;
    
    let arrayOfTasksPerPage = <?php echo json_encode($_SESSION["tasksperpage"], true); ?>;
    if (arrayOfTasksPerPage.length > 0) {
        let page = arrayOfTasksPerPage[1];
        for (let taskId of page) {
            document.getElementById(taskId + "li").style.display = "inline";
        }
    }
    $(".page-item").on("click", function() {
        if ($(this).attr("id") == "prev") {
            if (currentPage != 1) {
                let page = arrayOfTasksPerPage[currentPage];
                for (let taskId of page)
                    document.getElementById(taskId + "li").style.display = "none";
                page = arrayOfTasksPerPage[1];
                currentPage = 1;
                for (let taskId of page)
                    document.getElementById(taskId + "li").style.display = "inline";
            }
        } else if ($(this).attr("id") == "last") {
            if (currentPage != pages) {
                let page = arrayOfTasksPerPage[currentPage];
                for (let taskId of page)
                    document.getElementById(taskId + "li").style.display = "none";
                page = arrayOfTasksPerPage[pages];
                currentPage = pages;
                for (let taskId of page)
                    document.getElementById(taskId + "li").style.display = "inline";
            }
        } else {
            if ($(this).attr("id") != currentPage + "page") {
                let page = arrayOfTasksPerPage[currentPage];
                for (let taskId of page)
                    document.getElementById(taskId + "li").style.display = "none";
                for (let i = 0; i + "page" != $(this).attr("id"); ++i) 
                    currentPage = i;
                currentPage++;
                page = arrayOfTasksPerPage[currentPage];
                for (let taskId of page)
                    document.getElementById(taskId + "li").style.display = "inline";            
            }
        }
    });
    $("#usernamecolumn").on("click", function() {
        let data = callSortListFunction("username", currentPage, admin);
        data = data.split("||");
        arrayOfTasks = JSON.parse(data[1]);
        arrayOfTasksPerPage = JSON.parse(data[2]);
    });
    $("#emailcolumn").on("click", function() {
        let data = callSortListFunction("email", currentPage, admin, arrayOfTasks);
        data = data.split("||");
        arrayOfTasks = JSON.parse(data[1]);
        arrayOfTasksPerPage = JSON.parse(data[2]);
    });
    $("#statuscolumn").on("click", function() {
        let data = callSortListFunction("status", currentPage, admin, arrayOfTasks);
        data = data.split("||");
        arrayOfTasks = JSON.parse(data[1]);
        arrayOfTasksPerPage = JSON.parse(data[2]);
    });
    $(".doit-item").on("click", function() {
        arrayOfTasks = changeStatus($(this).attr("id"), arrayOfTasks);
        console.log(arrayOfTasks);
    });
    $(".edit-item").on("click", function() {
        arrayOfTasks = editTask($(this).attr("id"), arrayOfTasks);
        $("#edit-modal").modal('show');
    });
    $(".delete-item").on("click", function() {
        arr = deleteTask($(this).attr("id"), arrayOfTasks, currentPage, admin);
        arrayOfTasks = arr[0];
        arrayOfTasksPerPage = arr[1];
    });
    $("#savechanges").on("click", function() {
        let input = $("input")[0];
        let text = input.value;
        let id = input.getAttribute("id");
        input.setAttribute("id", "texttask");
        id = parseInt(id);
        let response = saveChanges(id, text);
        console.log(response);
        if (response != '0') {
            let currentTask = document.getElementById(id + "task-item");
            currentTask.innerHTML = "";
            currentTask.innerHTML = text;
            currentTask.setAttribute("class", "text-danger");
            for(let i in arrayOfTasks) {
                if (arrayOfTasks[i]["id"] == id){
                    arrayOfTasks[i]["task"] = text;
                    arrayOfTasks[i]["edited"] = 1;
                    break;
                }
            }
        } else {
            window.location.replace("authorization.php");
        }
        $("#edit-modal").modal('hide');
    });
});
</script>

                                                                