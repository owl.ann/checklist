function sortList(data, admin) {
    let ar = data.split("||");
    ar[1] = JSON.parse(ar[1]);
    ar[2] = JSON.parse(ar[2]);
    let max = $(".list-group-item").length;
    for (let i = max - 1; i != 0; i--) {
        $(".list-group-item")[i].remove();
    }
    arrayOfTasks = ar[1];
    createList(arrayOfTasks, admin);
    arrayOfTasksPerPage = ar[2];
    if (arrayOfTasksPerPage.length > 0) {
        let page = arrayOfTasksPerPage[ar[0]];
        for (let taskId of page) {
            document.getElementById(taskId + "li").style.display = "inline";
        }
    }
}

function callSortListFunction(columnname, currentPage, admin) {
        let sort;
        if ($("#" + columnname + "column").attr("sort") == "up") {
            sort = "down";
        } else {
            sort = "up";
        }
        let tasks;
        tasks = $.ajax({
            type: 'POST',
            url: "../logic/sortList.php",
            data: {column: columnname, sort: sort, page: currentPage},
            success: function (data) {
                sortList(data, admin);  
            },
            async:false
        }).responseText;
        $("#" + columnname + "column").attr("sort", sort);
        return tasks;
}	