function createDropDown(task) {
	let dropdownArrow = document.createElement("div");
	dropdownArrow.setAttribute("class", "col-1 dropdown");

	let a = document.createElement("a");
	a.setAttribute("class", "nav-link dropdown-toggle");
	a.setAttribute("href", "#");
	a.setAttribute("id", "navbarDropdownMenuLink");
	a.setAttribute("role", "button");
	a.setAttribute("data-toggle", "dropdown");
	a.setAttribute("aria-haspopup", "true");
	a.setAttribute("aria-expanded", "false");

	let menu = document.createElement("div");
	menu.setAttribute("class", "dropdown-menu");
	menu.setAttribute("aria-labelledby", "navbarDropdownMenuLink");

    let doitOption = null;
    if (task["done"] == 0) {
    	doitOption = document.createElement("p");
    	doitOption.setAttribute("class",  "dropdown-item doit-item");
    	doitOption.setAttribute("id", task["id"] + "doit");
    	doitOption.innerHTML = "do it";
    }

	let editOption = document.createElement("p");
	editOption.setAttribute("class", "dropdown-item edit-item");
	editOption.setAttribute("id", task["id"] + "edit");
	editOption.innerHTML = "edit";

	let deleteOption = document.createElement("p");
	deleteOption.setAttribute("class", "dropdown-item delete-item");
	deleteOption.setAttribute("id", task["id"] + "delete");
	deleteOption.innerHTML = "delete";

    if (doitOption)
	    menu.appendChild(doitOption);
	menu.appendChild(editOption);
	menu.appendChild(deleteOption);
	dropdownArrow.appendChild(a);
	dropdownArrow.appendChild(menu);
	
	return dropdownArrow;
}

function createList(arrayOfTasks, admin) {

	if (arrayOfTasks && arrayOfTasks.length > 0) {
		for (let task of arrayOfTasks) {
			let element = document.createElement("li");
			element.setAttribute("class", "list-group-item");
			element.setAttribute("id", task["id"] + "li");
			element.setAttribute("style", "display: none");

			let row = document.createElement("div");
			row.setAttribute("class","row");
            
            let dropdownArrow;
            if (admin)
			    dropdownArrow = createDropDown(task);
            else {
                dropdownArrow = document.createElement("div");
                dropdownArrow.setAttribute("class", "col-1 dropdown");
            }
            
            let status = document.createElement("div");
            status.setAttribute("class", "col-1");
            status.setAttribute("id", task["id"] + "status-item")

            if (task["done"] == 1)
                status.innerHTML = "+";
            else
                status.innerHTML = "-";

			let login = document.createElement("div");
			login.setAttribute("class", "col-2");
			login.innerHTML = task["name"];

			let email = document.createElement("div");
			email.setAttribute("class", "col-2");
			email.innerHTML = task["email"];

			let tasktext = document.createElement("div");
            if (task["edited"] == 1)
			    tasktext.setAttribute("class", "col-6 text-danger");
            else
                tasktext.setAttribute("class", "col-6");
			tasktext.setAttribute("id", task["id"] + "task-item");
			tasktext.innerHTML = task["task"];

			element.appendChild(row);
			row.appendChild(dropdownArrow);
            row.appendChild(status);
			row.appendChild(login);
			row.appendChild(email);
			row.appendChild(tasktext);

			$("#taskslist").append(element);
		}
	}
}
		