function changeStatus(id, arrayOfTasks) {
    taskId = parseInt(id);
    let task;
    for (let obj of arrayOfTasks) {
        if (obj["id"] == taskId) {
            obj["done"] = 1;
            break;
        }
    }
    $.ajax({
        type: "POST",
        url: "../logic/adminFunctions.php",
        data: {change: "status", id: taskId},
        success: function(data) {
            console.log(data);
        }
    });
    document.getElementById(taskId + "status-item").innerHTML = "+";
    $("#" + taskId + "doit").remove();
    return arrayOfTasks;
}

function editTask(id, arrayOfTasks) {
    let taskId = parseInt(id);
    let task;
    for (let obj of arrayOfTasks) {
        if (obj["id"] == taskId) {
            task = obj;
            break;
        }
    }

    let title = document.getElementById("modal-title");
    title.innerHTML = "";
    title.innerHTML = task["name"] + " " + task["email"];
    
    let texttask = document.getElementById("texttask");
    texttask.setAttribute("id", taskId + "texttask");
    texttask.setAttribute("value", "");
    texttask.setAttribute("placeholder", task["task"]);
    return arrayOfTasks;
}

function saveChanges(id, text) {
    let response = task = $.ajax({
       type: "POST",
       url: "../logic/adminFunctions.php",
       data: {change: "update", id: id, text: text},
       success: function(data) {},
       async: false
    }).responseText;
    return response;
}

function deleteTask(id, arrayOfTasks, currentPage, admin) {
    taskId = parseInt(id);
    for (let obj in arrayOfTasks) {
        if (arrayOfTasks[obj]["id"] == taskId) {
            arrayOfTasks.splice(obj, 1);
        }
    }
    $("#" + taskId + "li").remove();
    let data = $.ajax({
       type: "POST",
       url: "../logic/adminFunctions.php",
       data: {change: "delete", id: taskId},
       success: function(data) {
       },
       async: false
    }).responseText;
    arrayOfTasksPerPage = JSON.parse(data);
    let max = $(".list-group-item").length;
    for (let i = max - 1; i != 0; i--) {
        $(".list-group-item")[i].remove();
    }
    createList(arrayOfTasks, admin);
    if (arrayOfTasksPerPage.length > 0) {
        let page = arrayOfTasksPerPage[currentPage];
        for (let taskId of page) {
            document.getElementById(taskId + "li").style.display = "inline";
        }
    }
    
    return [arrayOfTasks, arrayOfTasksPerPage];
}				