<?php
session_name("checklist");
session_start();
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,
	shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<title><?php
		if ($_SERVER["REQUEST_METHOD"] != "GET" || empty($_GET["pagename"])) {
			echo "Checklist";
		} else {
			echo $_GET["pagename"];
		}
	?></title>
	<script>
		$(document).ready(function() {
			if ('<?php echo $_SESSION["logged_admin"]; ?>' == 'true') {
				let element = document.getElementById("logout");
				element.style.display = "inline";
			} else {
				let element = document.getElementById("login");
				element.style.display = "inline";
			}
		});
	</script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Checklist</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContente" aria-controls="navbarSupportedContent"
		aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="alltasks.php?pagename=All tasks">All tasks</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="createtask.php?pagename=CreateTask">New task</a>
				</li>
				<li id="login" class="nav-item active" style="display:none;">
					<a class="nav-link"
					href="authorization.php?pagename=Authorization">Log in</a>
				</li>
				<li id="logout" class="nav-item active" style="display:none;">
					<a class="nav-link" href="../logic/logout.php">Log out</a>
				</li>
			</ul>
		</div>
	</nav>
	