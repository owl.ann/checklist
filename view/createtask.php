<?php include('header.php'); ?>

<div class="container">
	<form method="post" action="../logic/saveTask.php">
		<fieldset class="border rounded p-2 m-3">
			<legend> Create Task </legend>
			<div class="form-group">
				<label for="InputUserName">Username</label>
				<input type="text" class="form-control" id="username"
				name="username" required/>
			</div>
			<div class="form-group">
				<label for="InputUserEmail">E-mail</label>
				<input type="email" class="form-control" id="useremail"
				name="email" required/>
			</div>
			<div class="form-group">
				<label for="InputTask">Task</label>
				<input type="text" class="form-control" id="task" name="task"
				required/>
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
		</fieldset>
	</form>
</div>

<?php include ('footer.html');
